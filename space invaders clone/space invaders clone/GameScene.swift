//
//  GameScene.swift
//  space invaders clone
//
//  Created by Souritra Das Gupta on 3/27/17.
//  Copyright © 2017 Souritra Das Gupta. All rights reserved.
//

import SpriteKit
import GameplayKit
var score = 0
class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let ship = SKSpriteNode(imageNamed: "ship")
    let invaders = SKNode()
    
    let scorelabel = SKLabelNode(fontNamed: "Pixel-Noir Caps")
    let bulletSound = SKAction.playSoundFileNamed("ShipBullet.wav", waitForCompletion: false)
    let invPop = SKAction.playSoundFileNamed("InvaderHit.wav", waitForCompletion: false)
    let shipPop = SKAction.playSoundFileNamed("ShipHit.wav", waitForCompletion: false)
    
    struct PhysicsCategories {
        static let None : UInt32 = 0
        static let Ship : UInt32 = 0b1
        static let Bullet : UInt32 = 0b10
        static let Invader : UInt32 = 0b100
    }
    
    override func didMove(to view: SKView) {
        
        score = 0
        self.physicsWorld.contactDelegate = self
        scorelabel.text = "Score: 0"
        scorelabel.fontSize = 10
        scorelabel.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        scorelabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left
        scorelabel.position = CGPoint(x: self.size.width/2 - 175, y: self.size.height - 65)
        scorelabel.zPosition = 10
        self.addChild(scorelabel)
        let bg = SKSpriteNode(color: #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), size: self.size)
        bg.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        bg.zPosition = 0
        self.addChild(bg)
        ship.position = CGPoint(x: self.size.width/2, y: self.size.height * 0.08)
        ship.physicsBody = SKPhysicsBody(rectangleOf: ship.size)
        ship.physicsBody?.affectedByGravity = false
        ship.zPosition = 4
        ship.physicsBody?.categoryBitMask = PhysicsCategories.Ship
        ship.physicsBody?.collisionBitMask = PhysicsCategories.None
        ship.physicsBody?.contactTestBitMask = PhysicsCategories.Invader
        self.addChild(ship)
        self.addChild(invaders)
        let inv10 = SKSpriteNode(imageNamed: "inv1")
        let inv11 = SKSpriteNode(imageNamed: "inv1")
        let inv12 = SKSpriteNode(imageNamed: "inv1")
        let inv13 = SKSpriteNode(imageNamed: "inv1")
        let inv14 = SKSpriteNode(imageNamed: "inv1")
        let inv15 = SKSpriteNode(imageNamed: "inv1")
        let inv20 = SKSpriteNode(imageNamed: "inv2")
        let inv21 = SKSpriteNode(imageNamed: "inv2")
        let inv22 = SKSpriteNode(imageNamed: "inv2")
        let inv23 = SKSpriteNode(imageNamed: "inv2")
        let inv24 = SKSpriteNode(imageNamed: "inv2")
        let inv25 = SKSpriteNode(imageNamed: "inv2")
        let inv30 = SKSpriteNode(imageNamed: "inv1")
        let inv31 = SKSpriteNode(imageNamed: "inv1")
        let inv32 = SKSpriteNode(imageNamed: "inv1")
        let inv33 = SKSpriteNode(imageNamed: "inv1")
        let inv34 = SKSpriteNode(imageNamed: "inv1")
        let inv35 = SKSpriteNode(imageNamed: "inv1")
        let inv40 = SKSpriteNode(imageNamed: "inv2")
        let inv41 = SKSpriteNode(imageNamed: "inv2")
        let inv42 = SKSpriteNode(imageNamed: "inv2")
        let inv43 = SKSpriteNode(imageNamed: "inv2")
        let inv44 = SKSpriteNode(imageNamed: "inv2")
        let inv45 = SKSpriteNode(imageNamed: "inv2")
        let inv50 = SKSpriteNode(imageNamed: "inv1")
        let inv51 = SKSpriteNode(imageNamed: "inv3")
        let inv52 = SKSpriteNode(imageNamed: "inv1")
        let inv53 = SKSpriteNode(imageNamed: "inv3")
        let inv54 = SKSpriteNode(imageNamed: "inv1")
        let inv55 = SKSpriteNode(imageNamed: "inv3")
        inv10.position = CGPoint(x: self.size.width/2 - 119, y: self.size.height - 250)
        inv11.position = CGPoint(x: inv10.position.x + inv10.size.width + 25, y: inv10.position.y)
        inv12.position = CGPoint(x: inv11.position.x + inv10.size.width + 25, y: inv10.position.y)
        inv13.position = CGPoint(x: inv12.position.x + inv10.size.width + 25, y: inv10.position.y)
        inv14.position = CGPoint(x: inv13.position.x + inv10.size.width + 25, y: inv10.position.y)
        inv15.position = CGPoint(x: inv14.position.x + inv10.size.width + 25, y: inv10.position.y)
        inv20.position = CGPoint(x: self.size.width/2 - 119, y: self.size.height - 285)
        inv21.position = CGPoint(x: inv20.position.x + inv20.size.width + 25, y: inv10.position.y - 35)
        inv22.position = CGPoint(x: inv21.position.x + inv20.size.width + 25, y: inv10.position.y - 35)
        inv23.position = CGPoint(x: inv22.position.x + inv20.size.width + 25, y: inv10.position.y - 35)
        inv24.position = CGPoint(x: inv23.position.x + inv20.size.width + 25, y: inv10.position.y - 35)
        inv25.position = CGPoint(x: inv24.position.x + inv20.size.width + 25, y: inv10.position.y - 35)
        inv30.position = CGPoint(x: self.size.width/2 - 119, y: self.size.height - 285 - 35)
        inv31.position = CGPoint(x: inv30.position.x + inv30.size.width + 25, y: inv20.position.y - 35)
        inv32.position = CGPoint(x: inv31.position.x + inv30.size.width + 25, y: inv20.position.y - 35)
        inv33.position = CGPoint(x: inv32.position.x + inv30.size.width + 25, y: inv20.position.y - 35)
        inv34.position = CGPoint(x: inv33.position.x + inv30.size.width + 25, y: inv20.position.y - 35)
        inv35.position = CGPoint(x: inv34.position.x + inv30.size.width + 25, y: inv20.position.y - 35)
        inv40.position = CGPoint(x: self.size.width/2 - 119, y: self.size.height - 285 - 70)
        inv41.position = CGPoint(x: inv40.position.x + inv40.size.width + 25, y: inv30.position.y - 35)
        inv42.position = CGPoint(x: inv41.position.x + inv40.size.width + 25, y: inv30.position.y - 35)
        inv43.position = CGPoint(x: inv42.position.x + inv40.size.width + 25, y: inv30.position.y - 35)
        inv44.position = CGPoint(x: inv43.position.x + inv40.size.width + 25, y: inv30.position.y - 35)
        inv45.position = CGPoint(x: inv44.position.x + inv40.size.width + 25, y: inv30.position.y - 35)
        inv50.position = CGPoint(x: self.size.width/2 - 119, y: self.size.height - 285 - 105)
        inv51.position = CGPoint(x: inv50.position.x + inv50.size.width + 25, y: inv40.position.y - 35)
        inv52.position = CGPoint(x: inv51.position.x + inv50.size.width + 25, y: inv40.position.y - 35)
        inv53.position = CGPoint(x: inv52.position.x + inv50.size.width + 25, y: inv40.position.y - 35)
        inv54.position = CGPoint(x: inv53.position.x + inv50.size.width + 25, y: inv40.position.y - 35)
        inv55.position = CGPoint(x: inv54.position.x + inv50.size.width + 25, y: inv40.position.y - 35)
        
        invaders.addChild(inv10)
        invaders.addChild(inv11)
        invaders.addChild(inv12)
        invaders.addChild(inv13)
        invaders.addChild(inv14)
        invaders.addChild(inv15)
        invaders.addChild(inv20)
        invaders.addChild(inv21)
        invaders.addChild(inv22)
        invaders.addChild(inv23)
        invaders.addChild(inv24)
        invaders.addChild(inv25)
        invaders.addChild(inv30)
        invaders.addChild(inv31)
        invaders.addChild(inv32)
        invaders.addChild(inv33)
        invaders.addChild(inv34)
        invaders.addChild(inv35)
        invaders.addChild(inv40)
        invaders.addChild(inv41)
        invaders.addChild(inv42)
        invaders.addChild(inv43)
        invaders.addChild(inv44)
        invaders.addChild(inv45)
        invaders.addChild(inv50)
        invaders.addChild(inv51)
        invaders.addChild(inv52)
        invaders.addChild(inv53)
        invaders.addChild(inv54)
        invaders.addChild(inv55)
        /*inv10.physicsBody = SKPhysicsBody(rectangleOf: inv10.size)
        inv10.physicsBody?.categoryBitMask = 2
        inv10.physicsBody?.collisionBitMask = 1
        inv10.physicsBody?.isDynamic = true
        inv10.physicsBody?.affectedByGravity = false
        inv10.zPosition = 2*/
        
        for child in invaders.children {
            child.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: inv10.size.width, height: 10))
            child.physicsBody?.usesPreciseCollisionDetection = true
            //child.physicsBody?.categoryBitMask = 2
            child.physicsBody?.collisionBitMask = PhysicsCategories.None
            child.physicsBody?.contactTestBitMask = PhysicsCategories.Ship | PhysicsCategories.Bullet
            child.physicsBody?.isDynamic = true
            child.physicsBody?.affectedByGravity = false
            child.zPosition = 2
            child.physicsBody?.categoryBitMask = PhysicsCategories.Invader
        }
        
        Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(GameScene.flyDown), userInfo: nil, repeats: true)
            }
    
    
    /*func touchDown(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.green
            self.addChild(n)
        }
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.blue
            self.addChild(n)
        }
    }
    
    func touchUp(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.red
            self.addChild(n)
        }
    }*/
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let bullet = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), size: CGSize(width: 3, height: 10))
        bullet.name = "bulletref"
        bullet.position = ship.position
        bullet.zPosition = 3
        bullet.physicsBody = SKPhysicsBody(rectangleOf: bullet.size)
        bullet.physicsBody?.usesPreciseCollisionDetection = true
        //bullet.physicsBody?.categoryBitMask = 1
        bullet.physicsBody?.collisionBitMask = PhysicsCategories.None
        bullet.physicsBody?.contactTestBitMask = PhysicsCategories.Invader
        bullet.physicsBody?.isDynamic = true
        bullet.physicsBody?.affectedByGravity = false
        bullet.physicsBody?.categoryBitMask = PhysicsCategories.Bullet
        self.addChild(bullet)
        let move = SKAction.moveTo(y: self.size.height + 11, duration: 2)
        let remove = SKAction.removeFromParent()
        let fire = SKAction.sequence([bulletSound, move, remove])
        bullet.run(fire)
        //invaders.position.y = invaders.position.y + 10
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t: AnyObject in touches{
            let touchpos = t.location(in: self)
            let prevtouchpos = t.previousLocation(in: self)
            let movement = touchpos.x - prevtouchpos.x
            ship.position.x += movement
            
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var body1 = SKPhysicsBody()
        var body2 = SKPhysicsBody()
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            body1 = contact.bodyA
            body2 = contact.bodyB
        }
        else {
            body1 = contact.bodyB
            body2 = contact.bodyA
        }
        if body1.categoryBitMask == PhysicsCategories.Ship && body2.categoryBitMask == PhysicsCategories.Invader {
        
            ship.run(invPop)
            ship.run(shipPop)
            body1.node?.removeFromParent()
            body2.node?.removeFromParent()
            gameover()
        }
        if body1.categoryBitMask == PhysicsCategories.Bullet && body2.categoryBitMask == PhysicsCategories.Invader {
        
            ship.run(invPop)
            body1.node?.removeFromParent()
            body2.node?.removeFromParent()
            score = score + 1
            scorelabel.text = "Score: \(score)"
            let scaleup = SKAction.scale(to: 1.2, duration: 0.1)
            let scaledown = SKAction.scale(to: 1, duration: 0.1)
            let bouncybouncy = SKAction.sequence([scaleup, scaledown])
            scorelabel.run(bouncybouncy)
        }
    }
    
    func flyDown() {
        invaders.position.y = invaders.position.y - 27
    }
    
    func gameover() {
        self.removeAllActions()
        self.enumerateChildNodes(withName: "bulletref"){
        
            bullet, stop in
            bullet.removeAllActions()
        }
        invaders.removeAllActions()
        ship.removeAllActions()
        let toScene = GOScene(size: self.size)
        toScene.scaleMode = self.scaleMode
        let move = SKTransition.fade(withDuration: 1)
        self.view!.presentScene(toScene, transition: move)
    }
    
    /*override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }*/
}
