//
//  GameOver.swift
//  space invaders clone
//
//  Created by Souritra Das Gupta on 3/31/17.
//  Copyright © 2017 Souritra Das Gupta. All rights reserved.
//

import Foundation
import SpriteKit

class GOScene: SKScene{
    let restartlabel = SKLabelNode(fontNamed: "Pixel-Noir Caps")
    override func didMove(to view: SKView) {
        let bg = SKSpriteNode(color: #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1), size: self.size)
        bg.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        bg.zPosition = 0;
        let label = SKLabelNode(fontNamed: "Pixel-Noir Caps")
        label.text = "Game Over"
        label.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        label.fontSize = 40
        label.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        label.zPosition = 1
        let fscore = SKLabelNode(fontNamed: "Pixel-Noir Caps")
        fscore.fontSize = 25
        fscore.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        fscore.position = CGPoint(x: self.size.width/2, y: label.position.y - 65)
        fscore.zPosition = 1
        fscore.text = "Score: \(score)"
        restartlabel.fontColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        restartlabel.text = "RESTART"
        restartlabel.fontSize = 20
        restartlabel.zPosition = 1
        restartlabel.position = CGPoint(x: self.size.width/2, y: fscore.position.y - 65)
        self.addChild(restartlabel)
        self.addChild(bg)
        self.addChild(label)
        self.addChild(fscore)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t: AnyObject in touches{
            let point = t.location(in: self)
            if restartlabel.contains(point) {
                let toScene = GameScene(size: self.size)
                toScene.scaleMode = self.scaleMode
                let move = SKTransition.fade(withDuration: 1)
                self.view!.presentScene(toScene, transition: move)
            }
        }
}
}
