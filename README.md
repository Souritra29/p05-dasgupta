# README #

### ATARI SPACE INVADERS CLONE ###

* Clone of the original Space Invaders by Atari. Prevent the ships from reaching earth. Shoot them down with the cannon.
* Version 1.1

### How do I get set up? ###

* Target OS - iOS 10.2
* Built with Swift 3 and SpriteKit
* All sound files and custom fonts are copied along with the game folder
* Screen size is 1344x750
* Tested on iPhone 6s simulator

### Contributors ###

* Souritra Das Gupta
* Contact Detection and using actions to switch over to other scenes and animation effects learned from Matt Heaney Apps on YouTube
* Pixel art, Sound files are from Ray Wenderleich's iOS tutorials

### Who do I talk to? ###

* Souritra Das Gupta
* B00671108
* sdasgup1@binghamton.edu